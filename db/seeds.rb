challenges = [["people", "Retention and attrition"],
["technology", "Knowledge Retention"],
["technology", "Training / Cross platform"],
["technology", "Technology change management"],
["communication", "Understanding"],
["diversities", "Time Zone"],
["engagement", "Cost Model"],
["engagement", "Contracts"],
["engagement", "Governance"],
["process", "Scalibility"],
["process", "Transperancy"],
["people", "Performance"],
["security", "Data integrity"],
["communication", "Quality of deliverables"],
["diversities", "Cultural Diversities"],
["engagement", "Change Management"],
["process", "Project lifecycle process"],
["security", "Data security"]]

challenges.each do|challenge|
  Challenge.create(header: challenge[0], name: challenge[1])
end
