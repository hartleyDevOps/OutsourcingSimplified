Rails.application.routes.draw do
  root :to => "home#outsourcing"
  get 'home/index'
  post 'home/create_challenges'
  get 'home/mitigation'
  post 'home/create_user'
  get 'home/generate_new_pdf'
  get 'mvp/new'
  post 'mvp/create'
  get 'home/outsourcing'
 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
