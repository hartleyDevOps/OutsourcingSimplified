class HomeController < ApplicationController
  def index
  	session.delete(:challenges_ids) if session[:challenges_ids].present?
  end

  def outsourcing
  end

  def create_challenges
    if params[:outsourceForm].blank?
      flash[:error] = "Please select atleast one challenge area." 
      redirect_to action: :index
      return
    end
  	if !session[:challenges_ids].present?
  		challenges = challenge_param.values.flatten
  		challenges_ids = Challenge.in(name: challenges).pluck(:id).map(&:to_s)
  	end
  	session[:challenges_ids] = challenges_ids
  	redirect_to  action: :mitigation
  end 

  def mitigation
  	if session[:challenges_ids].present? 
  		@challenge_ids = session[:challenges_ids]
  	else
  		flash[:notice] = 'Please select your challenges'
  		redirect_to root_path
  	end
  end

  def create_user
    attrs =  users_params.to_h
    u = User.new
    u.full_name = attrs[:name]
    u.phone = attrs[:phone] if attrs[:phone].present?
    u.company = attrs[:company]
    u.email = attrs[:email]
    u.ext = attrs[:ext] if attrs[:ext].present?
   
    if u.save
      challenges = Challenge.in(id: attrs["challenges_id"].split(' '))
      u.challenges << challenges
      session.delete(:challenges_ids)
      RiseMailer::email_them(u).deliver_now
      chal_hash = Hash.new
      list = challenges.pluck(:header, :name)
      list.map(&:first).each {|v| chal_hash[v] = []}
      list.each {|k,v| chal_hash[k] << v }

      respond_to do |format|
        format.pdf do
          render pdf: 'evaluation',
          layout: 'layouts/pdf.html.erb',
          locals: {chal_hash: chal_hash},
          background: true,
          no_background: false,
          footer:  {   html: {   template:'shared/footer.pdf.erb'}}
      end
    end
    else
      flash[:error] = u.errors.full_messages
      redirect_to home_mitigation_path
    end
  end

  protected

  def challenge_param
  	params.require(:outsourceForm).permit!
  end

  def users_params
    params.require(:userForm).permit!
  end


end
