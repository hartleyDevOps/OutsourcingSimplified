class MvpController < ApplicationController
	layout 'mvp'
	def new
	end

	def create
		attrs = mvp_params
		u = User.new
	    u.full_name = attrs[:name]
	    u.phone = attrs[:phone] if attrs[:phone].present?
	    u.company = attrs[:company]
	    u.email = attrs[:email]
	    u.ext = attrs[:ext] if attrs[:ext].present?
	    u.country = attrs[:country] if attrs[:country].present?
	    u.ebook = true

	    if u.save
	    	send_file(Rails.root.to_s + '/public/pdf-sample.pdf', :type => 'text/html; charset=utf-8')
	    else
	    	flash[:error] = u.errors.full_messages
	    	render :new 
	    end
	end

	protected

	def mvp_params
    	params.require(:mvpForm).permit!
  	end
end
