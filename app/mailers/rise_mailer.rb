class RiseMailer < ApplicationMailer
   
	def email_them(user)
		@user = user
		chal_hash = Hash.new
      	list = user.challenges.pluck(:header, :name)
      	list.map(&:first).each {|v| chal_hash[v] = []}
      	list.each {|k,v| chal_hash[k] << v }

		
  esign = WickedPdf.new.pdf_from_string(render_to_string  pdf: "esign",
 	  template: "layouts/pdf_mail.html.erb",
    formats: :html,
    encoding: "UTF-8",
    locals: {chal_hash: chal_hash},  
    background: true,
    no_background: false,
    footer: { 
      content: render_to_string(
        template: 'shared/footer.pdf.erb'
        )
      }
    ) 

    attachments['evaluation.pdf'] = esign

    mail(:to => @user.email, :subject => 'Hartley Lab - Outsourcing Mitigation', bcc: ['hello@hartleylab.com']) 
	end
end

