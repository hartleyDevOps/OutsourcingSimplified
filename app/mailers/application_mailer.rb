class ApplicationMailer < ActionMailer::Base
  default from: 'hello@hartleylab.com'
  layout 'mailer'
end
