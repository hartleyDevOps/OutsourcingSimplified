class User
  include Mongoid::Document
  include Mongoid::Timestamps

  field :email, type: String
  field :full_name,   type: String
  field :uuid,        type: String, default: ->{ SecureRandom.hex }
  field :phone, type: String
  field :company, type: String
  field :country, type: String
  field :ext, type: String
  field :ebook, type: Boolean

  has_many :challenges

  validates :full_name, presence: true
  validates :email,:presence => true, :format => { :with => /\A[^@\s]+@[^@\s]+\z/ }

 
end
