class Challenge
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,   type: String
  field :value,   type: String
  field :user_facing_text, type: String
  field :field_no, type: Integer
  field :user_id,	type: String
  field :header, type: String

  belongs_to :user, required: false
end
